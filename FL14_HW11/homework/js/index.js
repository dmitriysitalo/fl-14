// Your code goed here
let move = 0;
const winCombos = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[6, 4, 2]
]
let playerOneMoves = [];
let playerTwoMoves = [];

function addText (el) {
    move += 1;
    let point = document.getElementById(el.path[0].id)

    if (point.innerHTML === 'x') {
        return;
    }
    if (move % 2 === 0) {
        point.innerHTML = 'o';
        playerTwoMoves.push(el.path[0].id);
    } else {
        point.innerHTML = 'x';
        playerOneMoves.push(el.path[0].id);
    }
}

function removeText(el) {
    let point = document.getElementById(el.id)
    point.innerHTML = '';
}

function resetCell() {
    let cell = document.getElementsByClassName('td');
    for (let i = 0; i < cell.length; i++) {
        removeText(cell[i]);
    }
}

window.onload = function () {

    let cell = document.getElementsByClassName('td');
    for (let i = 0; i < cell.length; i++) {
        cell[i].addEventListener('click', addText)
    }
    let start = document.getElementById('start');
    start.addEventListener('click', resetCell);

    let reset = document.getElementById('reset');
    reset.addEventListener('click', resetCell);
}


