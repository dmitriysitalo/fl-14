const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");
const $remove = $('.item-remove');
const $text = $('.item-text');

function saveSettings() {
  if ($input.val() === '') {
    return;
  }
  let list = JSON.parse(localStorage.getItem('list'));
  list.push({text: $input.val(), done: false});
  localStorage.list = JSON.stringify(list);
  let obj = {text: $input.val(), done: false};
  render(obj);
  $input.val('');
}

function getSettings() {
  let list = JSON.parse(localStorage.getItem('list'));
  list.forEach(el => render(el))
}

function render(el) {
  let li = $('<li class="item"></li>');
  let span;
  if(el.done === true ) {
    span = $('<span class="item-text done"></span>').text(`${el.text}`);
  } else {
    span = $('<span class="item-text"></span>').text(`${el.text}`);
  }
  span.click(function() {
    $( this ).toggleClass( "done" );
    saveCurrent()
  });
  let button = $('<button class=\'item-remove\'></button>').text('Remove');
  li.append(span), li.append(button).on('click','button', deleteTask), $list.append(li);

}

function deleteTask() {
  let val = $(this).siblings().text();
  let newlist = JSON.parse(localStorage.getItem('list'));
  newlist.splice(newlist.findIndex(item => item.text === val),1);
  localStorage.list = JSON.stringify(newlist);
  $(this).parent().remove();
}

function saveCurrent() {
  let taskTextDone = $list.children('.item').children('.item-text.done');
  let taskText = $list.children('.item').children('.item-text');
  let list = [];
  if (taskTextDone.length !== 0) {
      taskTextDone.each(function(i, elem) {
        list.push({text: elem.innerHTML, done: true});
    })
  } 
  if (taskText.length !== 0) {
      taskText.each(function(i, elem) {
      if (elem.classList.contains('done')) {
       return; 
      } else {
        list.push({text: elem.innerHTML, done: false});
      }
      })
    }
  localStorage.list = JSON.stringify(list);
}

function renderBtnSearch() {
  let button = $('<button class=\'item-search\'></button>').text('Seach');
  button.click(function() {
    searchTask($input.val());
  });
  button.prop("type", "button");
  $('form').append(button);
}

function searchTask(el) {
  if(el) {
    let list = JSON.parse(localStorage.getItem('list'));
    list.forEach(localEl => {
      if(localEl.text === el) {
        $list.empty();
        render({text: el, done: false})
      }
    })
  } 
}

$.fn.todolist = function() {
  $add.prop("type", "button");
  $add.on("click", saveSettings);
  $remove.on('click', deleteTask);
  $text.click(function() {
    $( this ).toggleClass( "done" );
    saveCurrent()
  });
  saveCurrent();
  renderBtnSearch();
};

$(window).on('load', function() {
  $(window).todolist();
});