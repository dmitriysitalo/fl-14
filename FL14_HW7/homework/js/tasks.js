// 1
function maxElement (arr) {
    return Math.max.apply(null, arr);
}
//2
function copyArray (array) {
    return [...array];
}
//3
function addUniqueId(obj) {
    let id = Symbol(`${obj.name}`);
    let newObj = {
        name: obj.name,
        id: id
    }
    return newObj;
}
//4
function regroupObject({name: firstName, details: {id, age, university}}){
    return {university, user : { age, firstName, id}}
}
//5
function findUniqueELements(array) {
    let list = new Set();
    array.forEach(el => list.add(el));
    let result = [];
    for (let value of list) {
        result.push(value);
    }
    return result;
}
//6
function hideNumber (number) {
    let lastNumbers = 4;
    let result = number.slice(number.length - lastNumbers, number.length);
    result = result.padStart(number.length, '*');
    return result;
}
//7
const required = () => { 
    throw new Error(); 
};
const add = (a = required(), b = required()) => {     
    return a + b;
};
//8
fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(json => { 
      return render(json) 
    }
)

function render(json) {
    let list = [];
    json.forEach(el => {
        list.push(el.name);
    })
    return list.sort();
}
//9
let url = 'https://jsonplaceholder.typicode.com/users'
async function getData(url) {
    let backNumber = -1;
    let response = await fetch(url);
    let data = await response.json();
    return data.sort((a, b) => a.name > b.name ? 1 : backNumber);
}