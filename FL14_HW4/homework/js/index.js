let rankList = ['Ace', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King'];
let suitList = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];

class Deck {
    constructor(cards) {
        this.cards = cards;
    }
    get counts () {
        return this.cards.length; 
    }
    shuffle() {
        let index = this.cards.length;
        while (index !== 0) {
            let newIndex = Math.floor(Math.random() * index);
            index -= 1;
            let item = this.cards[index];
            this.cards[index] = this.cards[newIndex];
            this.cards[newIndex] = item;
        }
    }
    drawn(n) {
        let result = this.cards.splice(this.cards.length - n, n);
        return result;
    }
}

class Card {
    constructor(suit, rank) {
        this.suit = suit;
        this.rank = rank;
    }
    get isFaceCard() {
        let maxRank = 10;
        let minRank = 1;
        if (this.rank > maxRank || this.rank === minRank) {
            return true;
        } else {
            return false;
        }
    }
    toString() {
        return `${rankList[this.rank-1]} of ${suitList[this.suit]}`;
    }
    static compare(cardOne, cardTwo) {
        if (rankList.indexOf(cardOne) > rankList.indexOf(cardTwo)) {
            return cardOne;
        } else if (rankList.indexOf(cardOne) < rankList.indexOf(cardTwo)) {
            return cardTwo;
        } else {
            return false;
        }
    }
}

class Player {
    constructor(name, numberOfWins) {
        this.name = name;
        this.numberOfWins = numberOfWins;
    }
    get wins() {
        return this.numberOfWins;
    }
    set newWins(value) {
        this.numberOfWins = value;
    }
    static Play (playerOne, playerTwo) {

        let newDeckList = [];
        for (let i = 0; i < suitList.length; i++) {
            for (let j = 0; j < rankList.length; j++) {
                newDeckList.push(new Card(suitList[i], rankList[j]));
            }
        }
        let deck = new Deck(newDeckList);
        deck.shuffle();
        let numberOfPlayers = 2;
        let count = deck.counts / numberOfPlayers - 1;
        for (let i = 0; i <= count; i++) {
            let list = deck.drawn(numberOfPlayers);
            if(Card.compare(list[0].rank, list[1].rank) === list[0].rank) {
                let scoreOne = playerOne.wins + 1;
                playerOne.newWins = scoreOne;
            } else if (Card.compare(list[0].rank, list[1].rank) === list[1].rank) {
                let scoreTwo = playerTwo.wins + 1;
                playerTwo.newWins = scoreTwo;
            }
        }
        if (playerOne.wins > playerTwo.wins) {
            return `${playerOne.name} wins ${playerOne.wins} to ${playerTwo.wins}`;
        } else if (playerOne.wins < playerTwo.wins) {
            return `${playerTwo.name} wins ${playerTwo.wins} to ${playerOne.wins}`;
        } else {
            return 'draw!';
        }
    }
}

class Employee {
    constructor(
                id, 
                firstName, 
                lastName, 
                birthday, 
                salary,
                position, 
                department
                ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.salary = salary;
        this.position = position;
        this.department = department;
    }
    get age() {
        let minYear = 6;
        let maxYear = 10;
        let minMonth = 3;
        let maxMonth = 4;
        let date = new Date(this.birthday.slice(minYear, maxYear), 
            this.birthday.slice(minMonth, maxMonth), this.birthday.slice(0, 1))
        return getAge(date);
    }
    get fullName() {
        return this.firstName + ' ' + this.lastName;
    }
    static get EMPLOYEES() {
        if (this.list === undefined) {
            this.list = [];
        }
        return this.list;
    }
    quit () {
        return this.list.splice(this.list.indexOf(this.id), 1);
    }
    retire() {
        this.quit;
        return 'It was such a pleasure to work with you!';
    }
    getFired() {
        this.quit;
        return 'Not a big deal!';
    }
    changeDepartment(newDepartment) {
        this.department = newDepartment;
    }
    changePosition(newPosition) {
        this.position = newPosition;
    }
    changeSalary(newSalary) {
        this.salary = newSalary; 
    }
    getPromoted(benefits) {
        if (benefits.salary !== undefined) {
            this.salary = benefits.salary;
        }
        if (benefits.position !== undefined) {
            this.position = benefits.position;
        }
        if (benefits.department !== undefined) {
            this.department = benefits.department;
        }
        return 'Yoohooo!';
    }
    getDemoted(punishment) {
        if (punishment.salary !== undefined) {
            this.salary = punishment.salary;
        }
        if (punishment.position !== undefined) {
            this.position = punishment.position;
        }
        if (punishment.department !== undefined) {
            this.department = punishment.department;
        }
        return 'Damn!';
    }
}

function getAge(dBirthday) {
    let cDate = new Date();
    let dayBirthday = dBirthday.getDate();
    let monthBirthday = dBirthday.getMonth();    
    let cYear = cDate.getFullYear();
    let birthdayCurrentYear = new Date(cYear, monthBirthday, dayBirthday);

    if (birthdayCurrentYear - cDate < 0) {
        return cYear - +dBirthday.getFullYear();
    } else {
        return cYear - 1 - dBirthday.getFullYear();
    }
}

class Manager extends Employee {
    constructor(...args) {
        super(...args);
        this.position = 'manager';
    }
    get managedEmployees () {
        let list = Manager.EMPLOYEES.forEach(function(item) {
            if(item.department === this.department && item.position !== this.position) {
                return item;
            }
        })
        return list;
    }
}

class BlueCollarWorker extends Employee {}
class HRManager extends Manager {
    constructor(...args) {
        super(...args);
        this.position = 'hr';
    }
}

class SalesManager extends Manager {
    constructor(...args) {
        super(...args);
        this.position = 'sales';
    }
}

class ManagerPro extends Manager {
}