fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(json => render(json))

function render (json) {
  let table = document.createElement('table');
  table.className = 'table';
  document.body.appendChild(table);
  let tHead = document.createElement('thead');
  table.appendChild(tHead);
  let thId = document.createElement('th');
  thId.innerHTML = 'id';
  table.appendChild(thId);
  let thName = document.createElement('th');
  thName.innerHTML = 'Name';
  table.appendChild(thName);
  let thUserName = document.createElement('th');
  thUserName.innerHTML = 'User name';
  table.appendChild(thUserName);

  json.forEach(element => {
    let row = document.createElement('tr');
    table.appendChild(row);
    let tdId = document.createElement('td');
    tdId.innerHTML = element.id;
    tdId.id = `id-${element.id}`
    row.appendChild(tdId);
    let tdName = document.createElement('td');
    tdName.innerHTML = element.name;
    tdName.id = `name-${element.id}`
    tdName.dataset.id = element.id;
    tdName.onclick = function() {
      loadData(this);
    };  
    row.appendChild(tdName);
    let tdUserName = document.createElement('td');
    tdUserName.innerHTML = element.username;
    tdUserName.id = `userName-${element.id}`
    tdUserName.contentEditable = 'true';
    row.appendChild(tdUserName);
    let tdButtonSave = document.createElement('td');
    let buttonSave = document.createElement('button');
    buttonSave.className = 'save';
    buttonSave.id = element.id;
    buttonSave.innerHTML = 'Save';
    buttonSave.onclick = function() {
      saveChanges(this);
    };
    tdButtonSave.appendChild(buttonSave);
    row.appendChild(tdButtonSave);
    let tdButtonDel = document.createElement('td');
    let ButtonDel = document.createElement('button');
    ButtonDel.className = 'del';
    ButtonDel.id = element.id;
    ButtonDel.innerHTML = 'Delete';
    ButtonDel.onclick = function() {
      deleteEntry(this);
    };
    tdButtonDel.appendChild(ButtonDel);
    row.appendChild(tdButtonDel);
  })
}

function saveChanges(e) {
  let tdName = document.getElementById(`name-${e.id}`);
  let tdUserName = document.getElementById(`userName-${e.id}`)
  fetch(`https://jsonplaceholder.typicode.com/users/${e.id}`, {
    method: 'PUT',
    body: JSON.stringify({
        id: tdName.id,
        name: tdName.innerHTML,
        userName: tdUserName.innerHTML
      }),
    headers: {
        'Content-type': 'application/json; charset=UTF-8'
      }
    })
    .then((response) => response.json())
}

function deleteEntry(e) {
  fetch(`https://jsonplaceholder.typicode.com/users/${e.id}`, {
    method: 'DELETE'
  })
  let tdName = document.getElementById(`name-${e.id}`);
  tdName.parentNode.remove();
}

function loadData(e) {
  fetch(`https://jsonplaceholder.typicode.com/posts/${e.dataset.id}`)
  .then((response) => response.json())
  .then((json) => saveToLocal(json,'posts'))
  fetch(`https://jsonplaceholder.typicode.com/posts/${e.dataset.id}/comments`)
  .then((response) => response.json())
  .then((json) => saveToLocal(json,'comments'))
  getFromLocal();
}

function saveToLocal (data, type) {
  localStorage.setItem(`${type}`, JSON.stringify(data));
}

function getFromLocal () {
  let dataPost = '';
  dataPost += localStorage.getItem('posts');
  let dataComm = '';
  dataComm += localStorage.getItem('comments');
  document.body.innerHTML = `<p>${dataPost}</p><p>${dataComm}</p>`;
}