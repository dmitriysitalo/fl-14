let round = require('./round.js');
function startGame() {
    let btnList = document.getElementsByClassName('btn');
    for (let i = 0; i < btnList.length; i++) {
        btnList[i].addEventListener('onclick', round(btnList[i]));
    }

}
module.exports = startGame;
