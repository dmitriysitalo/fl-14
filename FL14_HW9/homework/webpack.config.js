const path = require('path');
const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;
const filename = (ext) => isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`;
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    entry: {
        app: ['./main.js', './js/start.js', './js/round.js']
    },
    output: {
        filename: `./js/${filename('js')}`,
        path: path.resolve(__dirname, './dir'),
    },
    devServer: {
        historyApiFallback: true,
        contentBase: path.resolve(__dirname, 'dir'),
        open: true,
        compress: true, 
        hot: true,
        port: 3000, 
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, 'src/index.html'),
            filename: 'index.html',
            minify: {
                collapseWhitespace: isProd
            }
        }),
        new CleanWebpackPlugin(),
    ],
}